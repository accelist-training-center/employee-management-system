﻿using ManagementWebApplication.Entities;
using ManagementWebApplication.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementWebApplication.Services
{
    public class EmployeeService
    {
        private readonly ManagementDbContext DB;

        public EmployeeService(ManagementDbContext dbContext)
        {
            this.DB = dbContext;
        }

        public async Task<bool> InsertEmployee(EmployeeModel employeeModel)
        {
            this.DB.Add(new Employee
            {
                EmployeeId = Guid.NewGuid(),
                Email = employeeModel.Email,
                Name = employeeModel.Name,
                PhoneNumber = employeeModel.PhoneNumber
            });

            await this.DB.SaveChangesAsync();
            return true;
        }

        public int GetTotalData()
        {
            var totalEmployee = this.DB
                .Employees
                .Count();

            return totalEmployee;
        }

        public async Task<List<EmployeeModel>> GetAllEmployeesAsync()
        {
            var employees = await this.DB
                .Employees
                .Select(Q => new EmployeeModel
                {
                    Email = Q.Email,
                    EmployeeId = Q.EmployeeId,
                    Name = Q.Name,
                    PhoneNumber = Q.PhoneNumber
                })
                .AsNoTracking()
                .ToListAsync();

            return employees;
        }

        public async Task<List<EmployeeModel>> GetAsync(int pageIndex, int itemPerPage, string filterByName)
        {
            // set employee to queryable for  filter
            var query = this.DB
                .Employees
                .AsQueryable();

            // for filter
            if (string.IsNullOrEmpty(filterByName) == false)
            {
                query = query
                    .Where(Q => Q.Name.StartsWith(filterByName));
            }

            // get data after filtering
            var employees = await query
                .Select(Q => new EmployeeModel
                {
                    EmployeeId = Q.EmployeeId,
                    Name = Q.Name,
                    Email = Q.Email,
                    PhoneNumber = Q.PhoneNumber
                })
                .Skip((pageIndex - 1) * itemPerPage)
                .Take(itemPerPage)
                .AsNoTracking()
                .ToListAsync();

            return employees;
        }

        public async Task<EmployeeModel> GetSpesificEmployeeAsync(Guid employeeId)
        {
            var employee = await this.DB
                .Employees
                .Where(Q => Q.EmployeeId == employeeId)
                .Select(Q => new EmployeeModel
                {
                    EmployeeId = Q.EmployeeId,
                    Name = Q.Name,
                    Email = Q.Email,
                    PhoneNumber = Q.PhoneNumber
                })
                .FirstOrDefaultAsync();

            return employee;
        }

        public async Task<bool> UpdateEmployeeAsync(EmployeeModel employee)
        {
            var employeeModel = await this.DB
                .Employees
                .Where(Q => Q.EmployeeId == employee.EmployeeId)
                .FirstOrDefaultAsync();

            if (employeeModel == null)
            {
                return false;
            }

            employeeModel.Email = employee.Email;
            employeeModel.Name = employee.Name;
            employeeModel.PhoneNumber = employee.PhoneNumber;

            await this.DB.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeleteEmployeeAsync(Guid employeeId)
        {
            var employeeModel = await this.DB.Employees.Where(Q => Q.EmployeeId == employeeId).FirstOrDefaultAsync();

            if (employeeModel == null)
            {
                return false;
            }

            this.DB.Remove(employeeModel);
            await this.DB.SaveChangesAsync();

            return true;
        }
    }
}
