﻿using ManagementWebApplication.Entities;
using ManagementWebApplication.Interfaces;
using Microsoft.EntityFrameworkCore;
using Minio;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementWebApplication.Services
{
    /// <summary>
    /// Store file using MinIO.
    /// </summary>
    public class MinIOService : IFileStorageService
    {
        private readonly MinioClient _minioClient;
        private readonly ManagementDbContext DB;

        public MinIOService(MinioClient mc, ManagementDbContext dbContext)
        {
            _minioClient = mc;
            this.DB = dbContext;
        }
        public async Task DeleteFileAsync(Guid fileName, string fileExtension)
        {
            //throw new NotImplementedException();
            var objectName = $"{fileName}.{fileExtension.ToLower()}";
            var bucketName = "EmployeeFile";
            await _minioClient.RemoveObjectAsync(bucketName, objectName);
        }

        public async Task<string> GetFileAsync(Guid blobId)
        {
            var extension = await this
                .DB
                .Blobs
                .AsNoTracking()
                .Select(Q => Q.Extension)
                .FirstOrDefaultAsync();

            var objectName = $"{blobId + extension}";
            var bucketName = "managementapp";
            var expiry = (int)TimeSpan.FromMinutes(15).TotalSeconds;
            return await _minioClient.PresignedGetObjectAsync(bucketName, objectName, expiry);
        }

        public async Task<Guid> InsertBlob(string name, string mime, string fileExtension)
        {
            var blob = new Blob
            {
                BlobId = Guid.NewGuid(),
                FilePath = name,
                MIME = mime,
                Extension = fileExtension
            };

            this.DB.Blobs.Add(blob);

            await this.DB.SaveChangesAsync();
            return blob.BlobId;
        }

        public async Task UploadFile(Guid blobId, string path, string mime, Stream data)
        {
            var ext = System.IO.Path.GetExtension(path).ToLower();
            var bucketName = "managementapp";
            var location = "us-east-1";
            var objectName = $"{blobId + ext}";

            bool found = await _minioClient.BucketExistsAsync(bucketName);
            if (!found)
            {
                await _minioClient.MakeBucketAsync(bucketName, location);
            }
            
            await _minioClient.PutObjectAsync(bucketName, objectName, data, data.Length);

            this.DB.Blobs.Add(new Blob
            {
                BlobId = blobId,
                FilePath = path,
                Extension = ext,
                MIME = mime
            });

        }
    }
}
