﻿using ManagementWebApplication.Entities;
using ManagementWebApplication.Interfaces;
using ManagementWebApplication.Models.Appsettings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementWebApplication.Services
{
    /// <summary>
    /// Store file using OS file system.
    /// </summary>
    public class FileSystemService : IFileStorageService
    {
        private readonly ManagementDbContext DB;

        public FileSystemService(ManagementDbContext db, IOptionsMonitor<Appsettings> fileSystemSettings)
        {
            this.DB = db;
            this.RootDirectory = fileSystemSettings.CurrentValue.FileSystem.RootDirectory;
        }

        public string RootDirectory { get; }

        public Task DeleteFileAsync(Guid fileName, string fileExtension)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetFileAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task<Guid> InsertBlob(string name, string mime, string fileExtension)
        {
            throw new NotImplementedException();
        }

        public async Task UploadFile(Guid blobId, string path, string mime, Stream data)
        {
            var ext = System.IO.Path.GetExtension(path).ToLower();

            if (Directory.Exists(this.RootDirectory) == false)
            {
                Directory.CreateDirectory(this.RootDirectory);
            }

            // Why use Path.Combine()?
            // Because each OS have different file system mechanisms.
            // Example: On Linux, to separate the directory, it will use "/". On Windows, it will use "\".
            // Because ASP.NET Core is cross-platform, hence this is why Path.Combine() is important.
            // Reference: https://docs.microsoft.com/en-us/dotnet/api/system.io.path.combine?view=netcore-3.1.
            var savePath = Path.Combine(this.RootDirectory, $"{blobId + ext}");

            this.DB.Blobs.Add(new Blob
            {
                BlobId = blobId,
                FilePath = savePath,
                Extension = ext,
                MIME = mime
            });

            using (var fs = File.Create(savePath))
            {
                // Reference: https://stackoverflow.com/a/5515894.
                data.Seek(0, SeekOrigin.Begin);
                await data.CopyToAsync(fs);
            }
        }
    }
}
