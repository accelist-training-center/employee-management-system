﻿using ManagementWebApplication.Entities;
using ManagementWebApplication.Interfaces;
using ManagementWebApplication.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementWebApplication.Services
{
    public class WorkplaceService
    {
        private readonly ManagementDbContext DB;

        private readonly IFileStorageService FsService;

        public WorkplaceService(ManagementDbContext db, IFileStorageService fsService)
        {
            this.DB = db;
            this.FsService = fsService;
        }

        public async Task<List<WorkplaceViewModel>> GetAllAsync()
        {
            var workplaces = await this
                .DB
                .Workplaces
                .AsNoTracking()
                .Select(Q => new WorkplaceViewModel
                {
                    WorkplaceId = Q.WorkplaceId,
                    Address = Q.Address,
                    BlobId = Q.BlobId
                })
                .ToListAsync();

            return workplaces;
        }

        public async Task CreateAsync(CreateWorkplaceModel newWorkplace)
        {
            var blobId = Guid.NewGuid();
            var photoPath = newWorkplace.Photo.FileName;
            var mime = newWorkplace.Photo.ContentType;

            // Always prefer to using Stream.
            // Be careful if you have to use MemoryStream or memory allocation functions!
            using (var stream = newWorkplace.Photo.OpenReadStream())
            {
                await this.FsService.UploadFile(blobId, photoPath, mime, stream);
            }
            
            this.DB.Workplaces.Add(new Workplace
            {
                WorkplaceId = Guid.NewGuid(),
                Address = newWorkplace.Address,
                BlobId = blobId
            });

            await this.DB.SaveChangesAsync();
        }
    }
}
