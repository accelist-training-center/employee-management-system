﻿using ManagementWebApplication.Entities;
using ManagementWebApplication.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementWebApplication.Services
{
    public class EmployeeAppService
    {
        private readonly ManagementDbContext DB;


        public EmployeeAppService(ManagementDbContext dbContext)
        {
            this.DB = dbContext;
        }

        public async Task<bool> InsertNewEmployeeApp(EmployeeModelApp employeeModel)
        {
            this.DB.Add(new EmployeeApp
            {
                Name = employeeModel.Name,
                Url = employeeModel.Url,
                CreatedBy = employeeModel.CreatedBy,
                CreatedAt = DateTimeOffset.Now.ToString("dd MM yyyy HH:mm:ss zzz"),
                UpdatedBy = employeeModel.CreatedBy,
                UpdatedAt = DateTimeOffset.Now.ToString("dd MM yyyy HH:mm:ss zzz"),
                EmployeeId = employeeModel.EmployeeId
            });

            await this.DB.SaveChangesAsync();
            return true;
        }

        public async Task<bool> InsertNewEmployeeAppModel(EmployeeAppModel employeeModel)
        {
            this.DB.Add(new EmployeeApp
            {
                Name = employeeModel.Name,
                Url = employeeModel.Url,
                EmployeeId = employeeModel.EmployeeId

            });

            await this.DB.SaveChangesAsync();
            return true;
        }

        public async Task<List<EmployeeModelApp>> GetAllEmployeeApp()
        {
            var employeeApp = await this.DB
                .EmployeeApps
                .Select(Q => new EmployeeModelApp
                {
                    Name = Q.Name,
                    Url = Q.Url,
                    CreatedBy = Q.CreatedBy,
                    CreatedAt = Q.CreatedAt,
                    UpdatedBy = Q.UpdatedBy,
                    UpdatedAt = Q.UpdatedAt,
                    EmployeeId = Q.EmployeeId
                })
                .AsNoTracking()
                .ToListAsync();

            return employeeApp;
        }

        public async Task<List<EmployeeModelApp>> GetAsync(Guid? id)
        {
            var employeeApps = await this.DB
                .EmployeeApps
                .Where(Q => Q.EmployeeId == id)
                .Select(Q => new EmployeeModelApp
                {
                    Name = Q.Name,
                    Url = Q.Url,
                    CreatedBy = Q.CreatedBy,
                    CreatedAt = Q.CreatedAt,
                    UpdatedBy = Q.UpdatedBy,
                    UpdatedAt = Q.UpdatedAt,
                    EmployeeId = Q.EmployeeId,
                    EmployeeAppId = Q.EmployeeAppId
                })
                .AsNoTracking()
                .ToListAsync();

            employeeApps = employeeApps
                .OrderByDescending(Q => DateTime.Parse(Q.UpdatedAt))
                .ToList();

            return employeeApps;
        }

        public async Task<bool> DeleteApp(int id)
        {
            var employeeApp = await this.DB
                .EmployeeApps
                .Where(Q => Q.EmployeeAppId == id)
                .FirstOrDefaultAsync();

            if (employeeApp == null)
            {
                return false;
            }
            this.DB.Remove(employeeApp);
            await this.DB.SaveChangesAsync();



            return true;
        }

        public async Task<EmployeeModelApp> GetSpecificAppAsync(int? id)
        {
            var apps = await this.DB
                .EmployeeApps
                .Where(Q => Q.EmployeeAppId == id)
                .Select(Q => new EmployeeModelApp
                {

                    EmployeeAppId = Q.EmployeeAppId,
                    EmployeeId = Q.EmployeeId,
                    Name = Q.Name,
                    Url = Q.Url,
                    CreatedAt = Q.CreatedAt,
                    CreatedBy = Q.CreatedBy,
                    UpdatedAt = Q.UpdatedAt,
                    UpdatedBy = Q.UpdatedBy
                })
                .FirstOrDefaultAsync();
            return apps;
        }

        public async Task<bool> UpdateAppAsync(EmployeeModelApp app)
        {
            var appModel = await this.DB.
                EmployeeApps
                .Where(Q => Q.EmployeeAppId == app.EmployeeAppId)
                .FirstOrDefaultAsync();
            if (appModel == null)
            {
                return false;

            }

            appModel.Name = app.Name;
            appModel.Url = app.Url;
            appModel.UpdatedAt = DateTimeOffset.Now.ToString("dd MM yyyy HH:mm:ss zzz");
            appModel.UpdatedBy = app.UpdatedBy;
            await this.DB.SaveChangesAsync();
            return true;
        }

        public async Task<List<GroupModel>> GetAllAsync()
        {
            var query = from employee in DB.Employees
                        join employeeApp in DB.EmployeeApps on employee.EmployeeId equals employeeApp.EmployeeId
                        select new EmployeeAppModel
                        {
                            EmployeeId = employee.EmployeeId,
                            Name = employee.Name,
                            Email = employee.Email,
                            AppName = employeeApp.Name,
                            Url = employeeApp.Url,
                            EmployeeAppId = employeeApp.EmployeeAppId
                        };
            var data = await query
                .AsNoTracking()
                .ToListAsync();

            var dataGrouping = data
                .GroupBy(Q => new
                {
                    Q.EmployeeId,
                    Q.Name,
                    Q.Email
                })
                .Select(Q => new GroupModel
                {
                    EmployeeId = Q.Key.EmployeeId,
                    Name = Q.Key.Name,
                    Apps = Q.Select(Z => new EmployeeModelApp
                    {
                        Name = Z.AppName,
                        EmployeeAppId = Z.EmployeeAppId,
                        Url = Z.Url

                    }).ToList()
                })
                .ToList();

            return dataGrouping;
        }
        public async Task<List<EmployeeAppModel>> GetAllAsync2()
        {
            var query = from employee in DB.Employees
                        join employeeApp in DB.EmployeeApps on employee.EmployeeId equals employeeApp.EmployeeId
                        select new EmployeeAppModel
                        {
                            EmployeeId = employee.EmployeeId,
                            Name = employee.Name,
                            Email = employee.Email,
                            AppName = employeeApp.Name,
                            Url = employeeApp.Url,
                            EmployeeAppId = employeeApp.EmployeeAppId
                        };
            var data = await query
                .AsNoTracking()
                .ToListAsync();

            return data;
        }
    }
}
