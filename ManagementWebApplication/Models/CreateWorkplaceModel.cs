﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementWebApplication.Models
{
    public class CreateWorkplaceModel
    {
        [MaxLength(4_000)]
        [Required]
        public string Address { get; set; }

        /// <summary>
        /// IFormFile is a specialized data type for storing blob / file data.
        /// Reference: https://docs.microsoft.com/en-us/aspnet/core/mvc/models/file-uploads?view=aspnetcore-3.1.
        /// </summary>
        [Required]
        public IFormFile Photo { get; set; }
    }
}
