﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementWebApplication.Models
{
    public class EmployeeAppModel
    {
        public string Name { get; set; }
        public Guid EmployeeId { get; set; }
        public string Email { get; set; }
        public string AppName { get; set; }
        public string Url { get; set; }
        public int EmployeeAppId { get; set; }
    }
}
