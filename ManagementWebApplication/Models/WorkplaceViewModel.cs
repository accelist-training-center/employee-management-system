﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementWebApplication.Models
{
    public class WorkplaceViewModel
    {
        public Guid WorkplaceId { get; set; }

        public string Address { get; set; }

        public Guid BlobId { get; set; }
    }
}
