﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementWebApplication.Models
{
    public class EmployeeModel
    {
        public Guid EmployeeId { set; get; }

        [Required]
        //[MaxLength(20), MinLength(5)]
        [StringLength(20, MinimumLength = 5)]
        [Display(Name = "Nama")]
        public string Name { set; get; }
         
        [Required]
        //[DataType(DataType.EmailAddress)]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { set; get; }

        [Required]
        [Display(Name="No telepon")]
        public string PhoneNumber { set; get; }
    }
}
