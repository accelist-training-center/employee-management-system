﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementWebApplication.Models
{
    public class EmployeeModelApp
    {
        public int EmployeeAppId { set; get; }
        [Required]
        public string Name { set; get; }
        [Required]
        public string Url { set; get; }
        public string CreatedBy { set; get; }
        public string CreatedAt { set; get; }
        public string UpdatedAt { set; get; }
        public string UpdatedBy { set; get; }
        public Guid EmployeeId { set; get; }

    }
}
