﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementWebApplication.Models
{
    public class GroupModel
    {
        public Guid EmployeeId { get; set; }

        public string Name { get; set; }


        public List<EmployeeModelApp> Apps { get; set; }
    }
}
