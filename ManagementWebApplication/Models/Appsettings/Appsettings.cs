﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementWebApplication.Models.Appsettings
{
    /// <summary>
    /// A model class that reflect the appsettings / configuration files JSON structure.
    /// </summary>
    public class Appsettings
    {
        public FileSystemSettings FileSystem { get; set; }
    }
}
