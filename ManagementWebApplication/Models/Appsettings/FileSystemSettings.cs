﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementWebApplication.Models.Appsettings
{
    /// <summary>
    /// A model class that reflect the FileSystem JSON structure in appsettings / configuration files.
    /// </summary>
    public class FileSystemSettings
    {
        public string RootDirectory { get; set; }
    }
}
