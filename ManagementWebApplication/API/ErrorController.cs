﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ManagementWebApplication.API
{
    /// <summary>
    /// A sample demo controller for throwing an exception or error.
    /// </summary>
    [Route("api/error")]
    public class ErrorController : Controller
    {
        [HttpGet]
        public ActionResult<string> Get()
        {
            throw new Exception("Sample error. Please do not access this again....");
        }
    }
}
