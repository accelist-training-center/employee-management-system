﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ManagementWebApplication.Entities;
using ManagementWebApplication.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ManagementWebApplication.API
{
    [Route("api/v1/blob")]
    public class BlobController : Controller
    {
        // It is not recommended to inject DbContext object in controller, because it might break the existing design pattern!
        private readonly ManagementDbContext DB;
        private readonly IFileStorageService FsStorageService;

        public BlobController(ManagementDbContext db, IFileStorageService fsStorageService)
        {
            this.DB = db;
            this.FsStorageService = fsStorageService;
        }

        // Uncomment if you want to implement MinIOService.
        [HttpGet("{blobId}")]
        public async Task<ActionResult> GetAsync(Guid blobId)
        {
            var result = await this.FsStorageService.GetFileAsync(blobId);

            return Redirect(result);
        }

        //Uncomment if you want to implement FileSystemService.
        //[HttpGet("{blobId}")]
        //public async Task<ActionResult> GetAsync(Guid blobId)
        //{
        //    var blob = await this
        //        .DB
        //        .Blobs
        //        .AsNoTracking()
        //        .Where(Q => Q.BlobId == blobId)
        //        .FirstOrDefaultAsync();

        //    // There should be no need to specify the directory location.
        //    // The directory location SHOULD be saved during Blob creation.

        //    var binary = new byte[0];

        //    using (var fs = System.IO.File.OpenRead(blob.FilePath))
        //    {
        //        using (var ms = new MemoryStream())
        //        {
        //            await fs.CopyToAsync(ms);

        //            // WARNING: Potential high memory consumption / memory leak in your web application.
        //            binary = ms.ToArray();
        //        }
        //    }

        //    return File(binary, blob.MIME, $"{blob.BlobId + blob.Extension}");
        //}
    }
}
