﻿export interface EmployeeModelTemp {
    employeeId?: string;
    name?: string;
    phoneNumber?: string;
    email?: string;
}

export interface EmployeeAppModelTemp {
    employeeId?: string;
    appName?: string;
    url?: string;
    createdBy?: string;
    employeeAppId?: number;
}
