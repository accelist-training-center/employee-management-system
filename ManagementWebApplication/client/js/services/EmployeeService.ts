﻿import Axios from 'axios';
import { EmployeeModelTemp } from '../models/IEmployeeModel'
import { ResponseModelTemp } from '../models/ResponseModelTemp';

export class EmployeeService {

    async getAllEmployee(): Promise<EmployeeModelTemp[]>{
        const response = await Axios.get<EmployeeModelTemp[]>('/api/v1/employee/all-employee');
        if (response.status === 200) {
            return response.data;
        }
        return [];
    }

    async insertEmployee(data: EmployeeModelTemp): Promise<string> {
        const response = await Axios.post<ResponseModelTemp>('/api/v1/employee/insert', data);
        if (response.status === 200) {
            return response.data.responseMessage
        }
        return response.data.responseMessage;
    }

    async updateEmployee(data: EmployeeModelTemp): Promise<string> {
        const response = await Axios.put<ResponseModelTemp>('/api/v1/employee/update', data);
        if (response.status === 200) {
            return response.data.responseMessage;
        }
        return response.data.responseMessage;
    }

    async deleteEmployee(data: string): Promise<string> {
        const response = await Axios.delete<ResponseModelTemp>('/api/v1/employee/delete-2/'+data);
        if (response.status === 200) {
            return response.data.responseMessage;
        }
        return response.data.responseMessage;
    }

    async getFilterData(pageIndex: number, itemPerPage: number, filterByName: string): Promise<EmployeeModelTemp[]> {
        const response = await Axios.get<EmployeeModelTemp[]>('/api/v1/employee/filter-data?pageIndex=' + pageIndex + '&itemPerPage=' + itemPerPage + '&filterByName=' + filterByName);
        if (response.status === 200) {
            return response.data;
        }

        return [];
    }

    async getTotalData(): Promise<number> {
        const response = await Axios.get<number>('/api/v1/employee/total-data');

        return response.data;
    }
}

export const EmployeeServiceSingleton = new EmployeeService();