using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ManagementWebApplication.Models;
using ManagementWebApplication.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ManagementWebApplication.Pages.Workplace
{
    public class IndexModel : PageModel
    {
        private readonly WorkplaceService WorkplaceService;

        public IndexModel(WorkplaceService workplaceService)
        {
            this.WorkplaceService = workplaceService;
        }

        public List<WorkplaceViewModel> Workplaces { get; set; }

        public async Task OnGetAsync()
        {
            this.Workplaces = await this.WorkplaceService.GetAllAsync();
        }
    }
}
