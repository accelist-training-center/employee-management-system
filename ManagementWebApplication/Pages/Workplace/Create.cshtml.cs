using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ManagementWebApplication.Models;
using ManagementWebApplication.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ManagementWebApplication.Pages.Workplace
{
    public class CreateModel : PageModel
    {
        private readonly WorkplaceService WorkplaceService;

        public CreateModel(WorkplaceService workplaceService)
        {
            this.WorkplaceService = workplaceService;
        }

        [BindProperty]
        public CreateWorkplaceModel CreateWorkplaceForm { get; set; }

        public void OnGet()
        {
        }

        public async Task<ActionResult> OnPostAsync()
        {
            if (ModelState.IsValid == false)
            {
                // Using TempData, you can pass the value to the Razor Page.
                // Reference: https://docs.microsoft.com/en-us/aspnet/core/fundamentals/app-state?view=aspnetcore-3.1.
                TempData["ErrorMessage"] = "Data is invalid.";
                return Page();
            }

            await this.WorkplaceService.CreateAsync(this.CreateWorkplaceForm);

            TempData["Message"] = "Successfully added the new data";
            return RedirectToPage();
        }
    }
}
