using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ManagementWebApplication.Models;
using ManagementWebApplication.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ManagementWebApplication.Pages.Employee
{
    public class IndexModel : PageModel
    {
        private readonly EmployeeService ServiceMan;

        public IndexModel(EmployeeService employeeService)
        {
            this.ServiceMan = employeeService;
        }

        public List<EmployeeModel> Employees { set; get; }

        // For filter 
        [Display(Name = "Search")]
        [BindProperty(SupportsGet = true)]
        public string FilterByName { get; set; }

        public int TotalPage { set; get; }
        [BindProperty(SupportsGet = true)]
        public int PageIndex { set; get; }
        public int ItemPerPage => 5;

        public async Task OnGetAsync()
        {
            if(PageIndex == 0)
            {
                PageIndex = 1;
            }

            // add param filterByName for filtering
            Employees = await ServiceMan.GetAsync(PageIndex, ItemPerPage, FilterByName);

            var totalEmployee = ServiceMan.GetTotalData();

            TotalPage = (int)Math.Ceiling(totalEmployee / (double)ItemPerPage);
        }

        public async Task<IActionResult> OnPostDeleteAsync(Guid id)
        {
            var isSuccess = await this.ServiceMan.DeleteEmployeeAsync(id);

            if (isSuccess == false)
            {
                return Page();
            }
            return Redirect("/employee");
        }
    }
}
