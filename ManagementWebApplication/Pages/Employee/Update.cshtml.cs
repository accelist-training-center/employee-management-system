using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ManagementWebApplication.Models;
using ManagementWebApplication.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ManagementWebApplication.Pages.Employee
{
    public class UpdateModel : PageModel
    {
        private readonly EmployeeService ServiceMan;

        public UpdateModel(EmployeeService employeeService)
        {
            this.ServiceMan = employeeService;
        }

        [BindProperty]
        public EmployeeModel CurrentEmployee { set; get; }

        public async Task OnGetAsync(Guid id)
        {
            CurrentEmployee = await ServiceMan.GetSpesificEmployeeAsync(id);
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid == false)
            {
                return Page();
            }

            var isExist = await ServiceMan.UpdateEmployeeAsync(CurrentEmployee);

            if(isExist == false)
            {
                return Page();
            }
            return Redirect("/employee");
        }
    }
}
