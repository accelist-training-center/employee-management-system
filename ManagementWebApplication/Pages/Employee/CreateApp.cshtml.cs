using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ManagementWebApplication.Models;
using ManagementWebApplication.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ManagementWebApplication.Pages.Employee
{
    public class CreateAppModel : PageModel
    {
        private readonly EmployeeAppService _serviceMan;

        [BindProperty]
        public EmployeeModelApp NewEmployeeApp { set; get; }


        public CreateAppModel(EmployeeAppService employeeAppService)
        {
            this._serviceMan = employeeAppService;
        }
        public void OnGet(Guid id)
        {
            NewEmployeeApp = new EmployeeModelApp();
            //buat masukkin employeeID ke textbox yang kita hidden
            NewEmployeeApp.EmployeeId = id;
        }
        public async Task<IActionResult> OnPost()
        {
            if(ModelState.IsValid == false)
            {
                return Page();
            }
            await _serviceMan.InsertNewEmployeeApp(NewEmployeeApp);
            return Redirect("/Employee");
        }
    }
}
