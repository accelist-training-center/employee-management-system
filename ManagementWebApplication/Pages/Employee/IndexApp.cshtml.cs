using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ManagementWebApplication.Models;
using ManagementWebApplication.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ManagementWebApplication.Pages.Employee
{
    public class IndexAppModel : PageModel
    {
        private readonly EmployeeAppService _serviceMan;
        public Guid IdEmployee { get; set; }

        public IndexAppModel(EmployeeAppService employeeAppService)
        {
            this._serviceMan = employeeAppService;
        }

        public List<EmployeeModelApp> employeeModelApps;
        public async Task OnGetAsync(Guid id)
        {
            this.employeeModelApps = await this._serviceMan.GetAsync(id);
            IdEmployee = id;
        }
        public async Task<IActionResult> OnPostDeleteAsync (int id)
        {
            var isSuccess = await this._serviceMan.DeleteApp(id);
            if(isSuccess==false)
            {
                return Page();
            }
            return Redirect("/Employee/Index");
           
        }

        
    }
}
