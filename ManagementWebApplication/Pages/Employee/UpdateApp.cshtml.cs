using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ManagementWebApplication.Models;
using ManagementWebApplication.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ManagementWebApplication.Pages.Employee
{
    public class UpdateAppModel : PageModel
    {
        private readonly EmployeeAppService _serviceApp;
        [BindProperty]
        public EmployeeModelApp CurrentApp { get; set; }

        public UpdateAppModel(EmployeeAppService employeeAppService)
        {
            this._serviceApp = employeeAppService;
        }

        public async Task OnGet(int id)
        {
            CurrentApp = await _serviceApp.GetSpecificAppAsync(id);

        }

        public async Task<IActionResult> OnPostAsync() {
            if (ModelState.IsValid == false)
            {
                return Page();
            }
            var isExist = await _serviceApp.UpdateAppAsync(CurrentApp);

            if(isExist == false)
            {
                return Page();
            }

            return Redirect("/employee/IndexApp");
        
        }


    }
}
