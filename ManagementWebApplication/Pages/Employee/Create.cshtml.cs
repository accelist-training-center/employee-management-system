using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ManagementWebApplication.Models;
using ManagementWebApplication.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ManagementWebApplication.Pages.Employee
{
    public class CreateModel : PageModel
    {
        private readonly EmployeeService ServiceMan;

        public CreateModel(EmployeeService employeeService)
        {
            this.ServiceMan = employeeService;
        }

        [BindProperty]
        public EmployeeModel NewEmployee { set; get; }
        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPost()
        {
            if(ModelState.IsValid == false)
            {
                return Page();
            }

            await ServiceMan.InsertEmployee(NewEmployee);
            return Redirect("./");
        }
    }
}
