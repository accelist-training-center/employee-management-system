using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ManagementWebApplication.Entities;
using ManagementWebApplication.Interfaces;
using ManagementWebApplication.Models.Appsettings;
using ManagementWebApplication.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Minio;

namespace ManagementWebApplication
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;

            // Inject IWebHostEnvironment, so we can access the the environment variables.
            this.Env = env;
        }

        public IConfiguration Configuration { get; }

        /// <summary>
        /// Store web host environment objects, such as environment variables.
        /// </summary>
        public IWebHostEnvironment Env { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var razorPageBuilder = services.AddRazorPages();

            // Check if the ASPNETCORE_ENVIRONMENT value is Development or not.
            if (this.Env.IsDevelopment() == true)
            {
                // Reference: https://docs.microsoft.com/en-us/aspnet/core/mvc/views/view-compilation?view=aspnetcore-3.1.
                razorPageBuilder.AddRazorRuntimeCompilation();
            }

            // Bind configuration into a class using options pattern.
            // Reference: https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-3.1#bind-to-a-class.
            services.Configure<Appsettings>(this.Configuration);

            services.AddDbContext<ManagementDbContext>(options =>
            {
                options.UseSqlite("Data Source=app.db");
            });

            services.AddTransient<EmployeeService>();

            services.AddTransient<EmployeeAppService>();

            services.AddTransient<WorkplaceService>();

            // Implement MinIOService.
            services.AddTransient<IFileStorageService, MinIOService>();

            // Implement FileSystemService.
            //services.AddTransient<IFileStorageService, FileSystemService>();

            services.AddSingleton<MinioClient>(di =>
            {
                // Reference: https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-3.1#hierarchical-configuration-data.
                var endpoint = this.Configuration.GetValue<string>("Minio:Endpoint");
                var accessKey = this.Configuration.GetValue<string>("Minio:AccessKey");
                var secretKey = this.Configuration.GetValue<string>("Minio:SecretKey");
                return new MinioClient(endpoint, accessKey, secretKey);
            });

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Version = "v1", Title = "Management API" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                // Error handling for development / debugging mode.
                // Will return the exception page which show the details of the error.
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // Error handling for non-development / non-debugging mode.
                // Typically used for staging & production environments.
                // Will return the Razor Page located at ./Pages/Error.cshtml (you can choose another Razor Page file too instead).
                app.UseExceptionHandler("/Error");
            }

            var scope = app.ApplicationServices.CreateScope();
            scope.ServiceProvider.GetRequiredService<ManagementDbContext>().Database.Migrate();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Management API");
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapControllers();
            });
        }
    }
}
