﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementWebApplication.Interfaces
{
    public interface IFileStorageService
    {
        Task<Guid> InsertBlob(string name, string mime, string fileExtension);

        Task UploadFile(Guid blobId, string path, string mime, Stream data);

        Task<string> GetFileAsync(Guid id);

        Task DeleteFileAsync(Guid fileName, string fileExtension);

    }
}
