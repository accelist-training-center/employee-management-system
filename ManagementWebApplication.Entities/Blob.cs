﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ManagementWebApplication.Entities
{
    /// <summary>
    /// Store blob metadata.
    /// </summary>
    public class Blob
    {
        [Key]
        public Guid BlobId { set; get; }

        [Required]
        public string FilePath { set; get; }

        [Required]
        public string MIME { set; get; }

        [Required]
        public string Extension { set; get; }
    }
}
