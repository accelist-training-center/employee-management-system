﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManagementWebApplication.Entities
{
    public class ManagementDbContext:DbContext
    {
        public ManagementDbContext(DbContextOptions<ManagementDbContext> options):base(options)
        {
        }

        public DbSet<Employee> Employees { set; get; }
        public DbSet<EmployeeApp> EmployeeApps { set; get; }
        public DbSet<Blob> Blobs { set; get; }

        public DbSet<Workplace> Workplaces { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>().ToTable("employee");
            modelBuilder.Entity<EmployeeApp>().ToTable("employeeApp");
            modelBuilder.Entity<Blob>().ToTable("blob");
            modelBuilder.Entity<Workplace>().ToTable("workplace");

            modelBuilder.Entity<EmployeeApp>()
                .Property(Q => Q.CreatedBy)
                .HasDefaultValue("SYSTEM");

            modelBuilder.Entity<EmployeeApp>()
                .Property(Q => Q.CreatedAt)
                .HasDefaultValueSql("Date('Now')");

            modelBuilder.Entity<EmployeeApp>()
                .Property(Q => Q.UpdatedAt)
                .HasDefaultValueSql("Date('Now')");

            modelBuilder.Entity<EmployeeApp>()
                .Property(Q => Q.UpdatedBy)
                .HasDefaultValue("SYSTEM");

            modelBuilder.Entity<EmployeeApp>()
                .HasOne(Q => Q.Employee)
                .WithMany(Q => Q.EmployeeApps)
                .HasForeignKey(Q => Q.EmployeeId)
                .HasConstraintName("FK_Employee_EmployeeApp");
        }
    }
}
