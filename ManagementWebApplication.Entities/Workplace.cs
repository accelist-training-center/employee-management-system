﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ManagementWebApplication.Entities
{
    /// <summary>
    ///  Employee working space.
    /// </summary>
    public class Workplace
    {
        [Key]
        public Guid WorkplaceId { get; set; }

        [Required]
        public string Address { get; set; }

        /// <summary>
        /// Store workplace photo reference ID.
        /// </summary>
        [Required]
        public Guid BlobId { get; set; }
    }
}
