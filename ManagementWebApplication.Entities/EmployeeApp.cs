﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ManagementWebApplication.Entities
{
    public class EmployeeApp
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmployeeAppId { set; get; }
        [Required]
        public string Name { set; get; }
        [Required]
        public string Url { set; get; }
        public string CreatedBy { set; get; }
        public string CreatedAt { set; get; }
        public string UpdatedAt { set; get; }
        public string UpdatedBy { set; get; }

        public Guid EmployeeId { set; get; }

        [ForeignKey("FK_Employee_EmployeeApp")]
        public Employee Employee { set; get; }
    }
}
